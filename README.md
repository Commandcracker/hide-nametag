# About Hide Nametag

Hide Nametag simply hides the Nametag from all players. \
You can configure all in the **plugins/HideNametag/[config.yml](https://gitlab.com/Commandcracker/hide-nametag/-/blob/master/src/main/resources/config.yml)** file.

## Requirements

Hide Nametag currently supports **CraftBukkit**, **Spigot** and [Paper](papermc.io/) **(recommended)**. \
Other server implementations may work, but we don't recommend them as they may cause compatibility issues.

## Links

|Source|Artifacts|Stats|
|:---:|:---:|:---:|
|[Gitlab](https://gitlab.com/Commandcracker/hide-nametag)|[Spigot](https://www.spigotmc.org/resources/hide-nametag.89244)|[bStats](https://bstats.org/plugin/bukkit/Hide%20Nametag/9984)|
| |[Bukkit](https://dev.bukkit.org/projects/hide-nametag)|
| |[CurseForge](https://www.curseforge.com/minecraft/bukkit-plugins/hide-nametag)|

## Metrics collection

Hide Nametag collects anonymous server statistics through bStats, an open-source Minecraft statistics service. \
\
[![bStats](https://bstats.org/signatures/bukkit/Hide%20Nametag.svg)](https://bstats.org/plugin/bukkit/Hide%20Nametag/9984) \
If you'd like to disable metrics collection via bStats, you can edit the **plugins/bStats/config.yml** file.
